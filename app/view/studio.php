<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Studio</title>
		<link rel="stylesheet" href="../../ressource/style/studio.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<meta name="viewport" content="width=device-width, initial-scale=1">
	</head>
	<body>
		<?php readfile("./header.php"); ?>
		<div class="studio-separator"></div>
		<div class="sideOne">
			<div class="webcam-container">
				<video id="webcam" autoplay="autoplay"></video>
			</div>
			<div id="but-container">
				<button class="c_submit c_studio">Take a Picture</button>
				<button class="c_submit c_studio">Upload a Picture</button>
			</div>
		</div>
		<div class="sideTwo">
			<div class="img-container">
				<div class="filter-container"></div>
				<div class="filter-container"></div>
				<div class="filter-container"></div>
				<div class="filter-container"></div>
				<div class="filter-container"></div>
			</div>
		</div>
	</body>
	<script type="text/javascript" src="../../ressource/script/script.js"></script>
	<!--	<script type="text/javascript" src="../../ressource/script/webcam.js"></script> -->
</html>
