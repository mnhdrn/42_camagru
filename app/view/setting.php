<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Settings</title>
		<link rel="stylesheet" href="../../ressource/style/general.css">
		<link rel="stylesheet" href="../../ressource/style/manage.css">
		<link rel="stylesheet" href="../../ressource/style/ui.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<meta name="viewport" content="width=device-width, initial-scale=1">
	</head>
	<body>
		<?php readfile("./header.php");?>
		<div class="set-separator"></div>
		<p class="set-name">Username's settings</p>
		<div class="set-container">
			<form action="controller-3.php" method="post">
				<hr/>
				<input class="c_input" type="text" name="login" placeholder="new Username">
				<hr/>
				<input class="c_input" type="password" name="psswd" placeholder="new password">
				<hr/>
				<input class="c_input" type="password" name="c_psswd" placeholder="confirm new password">
				<hr/>
				<span class="notif-container">
					<label class"switch_indic">notification:</label>
					<label class="switch">
						<input type="checkbox" checked>
						<span class="slider round"></span>
					</label>
				</span>
				<hr/>
				<hr/>
				<input class="c_submit" type="submit" value="change settings">
			</form>
			<div class="delete-container">
				<form  action="" method="post">
					<div class="delete-form">
						<input class="c_check_delete" type="checkbox" name="sure">
						<label for="sure">
							By checking this box you accepted that all your data gonna be deleted;
						</label>
					</div>
					<input class="c_submit c_submit_red" type="submit" value="Delete account">
				</form>
			</div>
			<hr>
		</div>
		<?php readfile("./footer.php"); ?>
	</body>
	<script type="text/javascript" src="../../ressource/script/script.js"></script>
</html>
