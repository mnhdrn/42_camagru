<footer>
	<div class="nav-credit">
		Camagru / Made by 
		<a href="https://profile.intra.42.fr/users/clrichar"><b>#Clrichar</b></a></div>
	<div class="nav-spacer"></div>
	<div class="nav-sign">
		<a href="" onclick="callSignin(); return false">sign in</a> /
		<a href="" onclick="callRegister(); return false">register</a>
	</div>
</footer>
