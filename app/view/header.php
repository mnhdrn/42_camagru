<header>
	<div class="header-container">
		<div class="nav-home"></div>
		<div class="nav-spacer"></div>
		<div class="nav-ui">
			<span id="go_gallery" class="nav-button fa fa-lg fa-globe"></span>
			<span id="go_studio" class="nav-button fa fa-lg fa-camera"></span>
			<span id="go_setting" class="nav-button fa fa-lg fa-cog"></span>
			<span id="go_signin" class="nav-button fa fa-lg fa-sign-in"></span>
			<!--
	   <span class="nav-button fa-sign-out"></span>
			-->
		</div>
	</div>
	<div class="header-hr"></div>
</header>
