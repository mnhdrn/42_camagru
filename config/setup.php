<?php
include "./database.php";

$db = "CREATE DATABASE IF NOT EXISTS camagru; USE camagru;";

$dbuser = "CREATE TABLE IF NOT EXISTS `users` (
`id` INT NOT NULL AUTO_INCREMENT,
`login` VARCHAR(24) NOT NULL,
`passwd` VARCHAR(255) NOT NULL,
`email` VARCHAR(255) NOT NULL,
`verified` BOOLEAN NOT NULL,
PRIMARY KEY (`id`));";

$dbphoto = "CREATE TABLE IF NOT EXISTS `photo` (
`id` INT NOT NULL AUTO_INCREMENT,
`path` TEXT NOT NULL,
`owner` VARCHAR(24) NOT NULL,
PRIMARY KEY (`id`));";

$dbcomment = "CREATE TABLE IF NOT EXISTS `comments` (
`id` INT NOT NULL AUTO_INCREMENT,
`user_id` INT NOT NULL,
`photo_id` INT NOT NULL,
PRIMARY KEY (`id`));";

$dblike = "CREATE TABLE IF NOT EXISTS `likes` (
`id` INT NOT NULL AUTO_INCREMENT,
`user_id` INT NOT NULL,
`photo_id` INT NOT NULL,
PRIMARY KEY (`id`));";

try {
  $pdo = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
}
catch(PDOException $ex){
  echo "Failed to connect to the database";
}

$pdo->exec($db);
$pdo->exec($dbuser);
$pdo->exec($dbphoto);
$pdo->exec($dbcomment);
$pdo->exec($dblike);

echo "Databases have been successfuly installed";
sleep(5);
header("location: ../index.php");
